document.getElementById('start-btn').addEventListener('click', function() {
    startGame();
    document.getElementById('word-input').focus();
});

document.getElementById('submit-btn').addEventListener('click', function() {
    submitWord();
});

document.getElementById('word-input').addEventListener('keydown', function(event) {
    if (event.key === 'Enter') {
        event.preventDefault(); // Impede o comportamento padrão do Enter (como submit de formulário)
        submitWord();
    }
});

function generateRandomSyllable() {
    // Carrega as sílabas do arquivo de texto
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'syllables.txt', false);
    xhr.send();

    if (xhr.status !== 200) {
        console.error('Erro ao carregar as sílabas do arquivo de texto.');
        return null;
    }

    // Divide o conteúdo do arquivo em linhas e remove espaços em branco
    var syllables = xhr.responseText.split('\n').map(syllable => syllable.trim());

    // Gera uma sílaba aleatória
    var randomIndex = Math.floor(Math.random() * syllables.length);
    return syllables[randomIndex];
}

var timer;
var timerDuration = 100; // Duração do temporizador em segundos
var score = 0;
var progressBar = document.getElementById('timer-progress');
var scoreDisplay = document.getElementById('score-display');

function startGame() {
    resetTimer();
    nextWord();
}

function nextWord() {
    var randomSyllable = generateRandomSyllable();
    document.getElementById('word-display').innerText = randomSyllable;
    document.getElementById('word-input').value = '';
}

function resetTimer() {
    clearInterval(timer);
    timerDuration = 100; // Defina a duração inicial do temporizador em segundos
    timer = setInterval(updateTimer, 100);
}

function updateTimer() {
    timerDuration--;

    // Atualiza a barra de progresso
    var progressPercentage = (timerDuration / 100) * 100;
    progressBar.style.width = progressPercentage + '%';
    console.log(progressPercentage);
    // Atualiza a pontuação com base na rapidez da resposta
    if (timerDuration > 0) {
        score += 10; // Ganha 10 pontos a cada segundo restante
    }

    scoreDisplay.innerText = 'Pontuação: ' + score;

    if (timerDuration <= 0) {
        endGame();
    }
}

function submitWord() {
    var enteredWord = document.getElementById('word-input').value;
    
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var wordlist = xhr.responseText.split('\n').map(word => word.trim().toLowerCase());

            // Verifica se a palavra está na lista
            if (wordlist.includes(enteredWord.toLowerCase()) && enteredWord.includes(document.getElementById('word-display').innerText) && enteredWord!="") {
                console.log('Palavra válida:', enteredWord);
                // Adiciona pontos com base na rapidez
                score += 50; // Ganha 50 pontos por palavra correta
                resetTimer();
                nextWord();
            } else {
                console.log('Palavra inválida:', enteredWord);
                document.getElementById('word-input').classList.add('shake');
                setTimeout(() => {
                  document.getElementById('word-input').classList.remove('shake');
                }, 500);
            }

            // Limpar o campo de entrada após a submissão
            document.getElementById('word-input').value = '';
        }
    };

    xhr.open('GET', 'wordlist.txt', true);
    xhr.send();
}

function endGame() {
    console.log('Tempo esgotado! Fim do jogo. Pontuação final:', score);
    clearInterval(timer);

    // Adicione uma mensagem no site
    document.getElementById('word-display').innerText = 'Tempo esgotado! Fim do jogo. Pontuação final: ' + score;
    document.getElementById('start-btn').innerText = "Jogar novamente";
    score = 0; // Reinicia a pontuação
    scoreDisplay.innerText = 'Pontuação: ' + score;
}
