
<?php
  require "credentials.php";


  
    // Create connection
  $conn = mysqli_connect($servername, $user, $pass, $dbname);
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }

  // Use database
  $sql = "use $dbname";
  if (mysqli_query($conn, $sql)) {
      //echo "Database selected successfully<br>";
  } else {
      echo "Error selecting database: " . mysqli_error($conn) . "<br>";
  }

  if (isset($_POST['email']) || isset($_POST['password'])) {

    if(strlen($_POST['email']) == 0) {
      echo "Preencha o seu e-mail.";
    } else if (strlen($_POST['password']) == 0) {
      echo "Preencha a sua senha.";
    } else {

        $email = $conn->real_escape_string($_POST['email']);
        $password = $conn->real_escape_string($_POST['password']);

        echo $email;
        echo $password;

    }

  }

  mysqli_close($conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Acesse a sua conta</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    
    <link rel="stylesheet" href="styles.css">
</head>
<body>

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h1 class="text-center mb-4">Login</h1>
            <form action="" method="POST">  
                <div class="mb-3">
                    <label for="username" class="form-label">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" required>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Senha</label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>
                <div class="mb-3 text-center">
                    <button type="submit" class="btn btn-warning">Entrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script src="script.js"></script>
</body>
</html>
