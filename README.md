# Type It! - Descrição do Projeto

## Visão Geral

O **Type It!** é um projeto desenvolvido para aprimorar as habilidades de digitação dos usuários. O jogo desafia os jogadores a digitar palavras apresentadas na tela dentro de um limite de tempo. 

## Interface do Usuário

### Título e Boas-vindas

- O título "Type It!" e uma mensagem de boas-vindas são exibidos no topo da página.

### Iniciar Jogo

- Uma sílaba aleatória é gerada e exibida no centro da tela como a palavra que o jogador deve digitar.
- Um campo de entrada de texto ("Digite a palavra...") é apresentado para que o jogador insira a palavra correspondente à sílaba gerada.

### Temporizador e Barra de Progresso

- Um temporizador é iniciado quando o jogo começa, limitando o tempo disponível para digitar a palavra.
- Uma barra de progresso visualiza o tempo restante.

### Pontuação

- A pontuação do jogador é exibida durante o jogo, sendo atualizada com base no tempo que o jogador consegue sobreviver.

### Fim do Jogo

- Quando o temporizador atinge zero, o jogo é encerrado e a pontuação final é exibida.
- O botão "Jogar Novamente" é apresentado, permitindo que o jogador reinicie o jogo.

## Geração de sílabas e Validação do Input

- **Atalho de Enter:** Os jogadores podem pressionar a tecla Enter para enviar a palavra, além de clicar no botão "Enviar" (que está invisível, mas existe).
- **Lista de Palavras:** As palavras válidas são carregadas a partir de um arquivo de lista de palavras (`wordlist.txt`).
- **Sílabas Aleatórias:** As sílabas utilizadas no jogo são carregadas de um arquivo (`syllables.txt`) para garantir diversidade.

## Tecnologias Utilizadas

- **HTML, CSS, Bootstrap:** Para estruturação e estilo da página.
- **JavaScript:** Para interatividade e manipulação do DOM.
- **XMLHttpRequest:** Para fazer requisições assíncronas ao carregar listas de palavras e sílabas.
- **Temporizador:** Implementado para controlar o tempo de jogo.
- **Feedback Visual:** Animações CSS (classe 'shake') para fornecer feedback visual ao jogador.

# Autoria de Tiago Pareja Vita