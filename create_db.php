<?php
  require('credentials.php');

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  
  // Create connection
  $conn = mysqli_connect($servername, $user, $pass);

  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }

  // SELECT database
  $sql = "use $dbname";
  if (mysqli_query($conn, $sql)) {
      echo "Database selected successfully<br>";
  } else {
      echo "Error selecting database: " . mysqli_error($conn) . "<br>";
  }

  // Drop existing tables (be careful with this in a real scenario)
  $tables_to_drop = ["Participates", "Game", "Users", "League"];
  foreach ($tables_to_drop as $table) {
    $sql = "DROP TABLE IF EXISTS $table";
    if (mysqli_query($conn, $sql)) {
        echo "Table '$table' dropped successfully<br>";
    } else {
        echo "Error dropping table '$table': " . mysqli_error($conn) . "<br>";
    }
  }

  // Create Users table
  $sql = "
    CREATE TABLE Users (
      id_user INTEGER PRIMARY KEY,
      username VARCHAR(20),
      email VARCHAR(100),
      password VARCHAR(30),
      alltime_score INTEGER,
      week_score INTEGER,
      UNIQUE (id_user, username, email)
    )";
  if (mysqli_query($conn, $sql)) {
      echo "Table 'Users' created successfully<br>";
  } else {
      echo "Error creating table 'Users': " . mysqli_error($conn) . "<br>";
  }

  // Create Game table
  $sql = "
    CREATE TABLE Game (
      id_game INTEGER PRIMARY KEY,
      date DATE,
      score INTEGER,
      fk_Users_id_user INTEGER,
      FOREIGN KEY (fk_Users_id_user) REFERENCES Users (id_user) ON DELETE CASCADE
    )";
  if (mysqli_query($conn, $sql)) {
      echo "Table 'Game' created successfully<br>";
  } else {
      echo "Error creating table 'Game': " . mysqli_error($conn) . "<br>";
  }

  // Create League table
  $sql = "
    CREATE TABLE League (
      id_league INTEGER PRIMARY KEY,
      code INTEGER,
      name VARCHAR(30),
      alltime_lscore INTEGER,
      week_lscore INTEGER,
      UNIQUE (id_league, code, name)
    )";
  if (mysqli_query($conn, $sql)) {
      echo "Table 'League' created successfully<br>";
  } else {
      echo "Error creating table 'League': " . mysqli_error($conn) . "<br>";
  }

  // Create Participates table
  $sql = "
    CREATE TABLE Participates (
      fk_Users_id_user INTEGER,
      fk_League_id_league INTEGER,
      PRIMARY KEY (fk_Users_id_user, fk_League_id_league)
    )";
  if (mysqli_query($conn, $sql)) {
      echo "Table 'Participates' created successfully<br>";
  } else {
      echo "Error creating table 'Participates': " . mysqli_error($conn) . "<br>";
  }

   // Alter tables
  $sql = "
    ALTER TABLE Participates ADD CONSTRAINT FK_Participates_1
     FOREIGN KEY (fk_Users_id_user)
     REFERENCES Users (id_user)
     ON DELETE CASCADE;
    ";
  if (mysqli_query($conn, $sql)) {
      echo "Table 'Participates' altered successfully<br>";
  } else {
      echo "Error altering table 'Participates (USERS FK)': " . mysqli_error($conn) . "<br>";
  }

   // Alter tables
  $sql = "
    ALTER TABLE Participates ADD CONSTRAINT FK_Participates_2
     FOREIGN KEY (fk_League_id_league)
     REFERENCES League (id_league)
     ON DELETE CASCADE;
    ";
  if (mysqli_query($conn, $sql)) {
      echo "Table 'Participates' altered successfully<br>";
  } else {
      echo "Error altering table 'Participates (LEAGUE FK)': " . mysqli_error($conn) . "<br>";
  }


  mysqli_close($conn);
?>
